# Locate LuaJIT library
# This module defines
#  LUAJIT_FOUND, if false, do not try to link to Lua
#  LUA_INCLUDE_DIR, where to find lua.h
#  LUA_VERSION_STRING, the version of Lua found (since CMake 2.8.8)
#
# This module is similar to FindLua51.cmake except that it finds LuaJit instead.

find_package (PkgConfig REQUIRED)
pkg_check_modules (LuaJIT QUIET REQUIRED luajit)
set (LUA_LIBRARY ${LuaJIT_LIBRARIES})
set (LUA_INCLUDE_DIR ${LuaJIT_INCLUDE_DIRS})
set (LUA_VERSION_STRING ${LuaJIT_VERSION})

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LUAJIT_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LuaJIT
	REQUIRED_VARS LUA_LIBRARY LUA_INCLUDE_DIR
	VERSION_VAR LUA_VERSION_STRING)

MARK_AS_ADVANCED(LUA_INCLUDE_DIR LUA_LIBRARY LUA_MATH_LIBRARY)
