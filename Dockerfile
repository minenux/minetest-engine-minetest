FROM debian:stretch

USER root
RUN apt-get update -y && \
	apt-get -y install build-essential libirrlicht-dev cmake libbz2-dev libpng-dev libjpeg-dev \
		libsqlite3-dev libcurl4-gnutls-dev zlib1g-dev libgmp-dev libjsoncpp-dev libidn11-dev git

COPY . /usr/src/minetest5

RUN	mkdir -p /usr/src/minetest5/cmakebuild && cd /usr/src/minetest5/cmakebuild && \
    	cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release -DRUN_IN_PLACE=FALSE \
		-DBUILD_SERVER=TRUE \
		-DBUILD_CLIENT=FALSE \
		-DENABLE_SYSTEM_JSONCPP=1 \
		.. && \
		make -j2 && \
		rm -Rf ../games/minetest_game && git clone --depth 1 https://github.com/minetest/minetest_game ../games/minetest_game && \
		rm -Rf ../games/minetest_game/.git && \
		make install

FROM debian:stretch

USER root
RUN groupadd minetest5 && useradd -m -g minetest5 -d /var/lib/minetest5 minetest5 && \
    apt-get update -y && \
    apt-get -y install libcurl3-gnutls libjsoncpp1 liblua5.1-0 libluajit-5.1-2 libpq5 libsqlite3-0 \
        libstdc++6 zlib1g libc6 && \
    apt-get clean && rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /var/lib/minetest5

COPY --from=0 /usr/local/share/minetest5 /usr/local/share/minetest5
COPY --from=0 /usr/local/bin/minetest5server /usr/local/bin/minetest5server
COPY --from=0 /usr/local/share/doc/minetest5/minetest5.conf.example /etc/minetest5/minetest5.conf

USER minetest5

EXPOSE 30000/udp

CMD ["/usr/local/bin/minetest5server", "--config", "/etc/minetest5/minetest5.conf"]
