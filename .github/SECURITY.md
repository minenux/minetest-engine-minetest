# Security Policy

## Supported Versions

We only support the minenux stable version for security issues.
See the [releases page](https://github.com/minenux/minetest-engine-minetest/releases).

## Reporting a Vulnerability

We ask that you report vulnerabilities only using telegram group: https://t.me/venenux_minetest_bot
to give us time to fix them.

Depending on severity, we will either create a private issue for the vulnerability
and release a patch version of Minetest, or give you permission to file the issue publicly.

For more information on the justification of this policy, see
[Responsible Disclosure](https://en.wikipedia.org/wiki/Responsible_disclosure).
